# imgfunc

#### 介绍

图片压缩上传

#### 软件架构

软件架构说明

#### 安装教程

1.  `npm install imgfunc`
2.  `<script src="@imgfunc/dist/index.min.js"></script>`

#### 使用说明

```javascript
import UploadImg from 'imgfunc';
UploadImg({
   fileType: /\/(?:jpeg|png|gif|jpg)/i, // 上传图片类型限制
    changeType: "image/jpeg", // 后台需要的图片类型，方便压缩默认全部转jpeg
    elem: null, // input type="file" 节点,用来获取文件
    base64: null, // 图片文件 database64,一般是非input 获取的文件传
    maxSize: 2*1024*1024, // 图片文件最大值 以字节（B）为单位 。默认 2M
    multiple:false,// 是否多文件（暂不支持）
    uploadUrl, // 上传地址
    xhrParam, // 上传所需参数，必须是 Object
    uploadTimeout:60, // 超时时间（只计算执行接口的超时）
    formdataName, // 上传文件的name
    beforeUpload, // 上传前调用事件，此方法可以拦截上传，以及获取图片base64
    uploading,// 正在执行上传事件
    successUpload, // 上传成功回调
    errorUpload, // 上传失败回调
    fileName:'blob', // 上传文件名称 用于new FormDate() append方法第三个参数, default:'blob'
})
```

#### 参与贡献

1.  [Fork 本仓库](https://gitee.com/xunyu/uplaodImg.git)
