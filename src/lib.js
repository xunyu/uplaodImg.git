/**
 * 计算base64长度
 * @param {DataBase} base64
 */
export const calcBASE64length = base64 =>
  base64.length - (base64.length / 8) * 2;
