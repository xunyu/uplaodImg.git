import { IMAGEERROR } from "./errorType";
import {STATIC_EXCHANGE_ERROR} from './statusEnum'

export default (base64, callback) =>
  new Promise((resolve, reject) => {
    let image = new Image();
    image.onload = () => {
      resolve(image);
      image = null;
    };
    // 图片加载出错
    image.onerror = () => {
      reject({
        message: "文件转换图片出错",
        errorType: IMAGEERROR,
        status: STATIC_EXCHANGE_ERROR
      });
    };
    image.src = base64;
  });
