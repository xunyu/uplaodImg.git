export const FILETYPE = "FILETYPE"; // 文件类型错误

export const IMAGEERROR = "IMAGEERROR"; // 转换图片文件错误

export const COMPRESSERROR = "COMPRESSERROR"; // 图片文件压缩错误

export const BLOBCHANGEERROR = "BLOBCHANGEERROR"; // 图片上传出错

export const UPLOADERROR = "UPLOADERROR"; // 图片上传出错
