import nodeResolve from "rollup-plugin-node-resolve";
import babel from "rollup-plugin-babel";
import replace from "rollup-plugin-replace";
import { terser } from "rollup-plugin-terser";
import commonjs from "rollup-plugin-commonjs";

import pkg from "./package.json";

const makeExternalPredicate = externalArr => {
  if (externalArr.length === 0) {
    return () => false;
  }
  const pattern = new RegExp(`^(${externalArr.join("|")})($|/)`);
  return id => pattern.test(id);
};

export default [
  // CommonJS
  {
    input: "src/index.js",
    output: { file: "lib/index.js", format: "cjs", indent: false },
    external: makeExternalPredicate([
      ...Object.keys(pkg.dependencies || {}),
      ...Object.keys(pkg.peerDependencies || {})
    ]),
    plugins: [
      nodeResolve({}),
      babel({ exclude: "node_modules/**", runtimeHelpers: true })
    ]
  },

  // ES
  {
    input: "src/index.js",
    output: { file: "es/index.js", format: "es", indent: false },
    external: makeExternalPredicate([
      ...Object.keys(pkg.dependencies || {}),
      ...Object.keys(pkg.peerDependencies || {})
    ]),
    plugins: [nodeResolve({})]
  },

  // UMD Development
  {
    input: "src/index.js",
    output: {
      file: "dist/index.js",
      format: "umd",
      name: "UploadImg",
      indent: false
    },
    plugins: [
      nodeResolve({}),
      babel({
        exclude: "node_modules/**",
        runtimeHelpers: true
      }),
      commonjs({
        include: "node_modules/**",
        sourceMap: true
      }),
      replace({
        "process.env.NODE_ENV": JSON.stringify("development")
      })
    ]
  },

  // UMD Production
  {
    input: "src/index.js",
    output: {
      file: "dist/index.min.js",
      format: "iife",
      name: "UploadImg",
      indent: false
    },
    plugins: [
      nodeResolve({}),
      babel({
        exclude: "node_modules/**",
        runtimeHelpers: true
      }),
      commonjs({ include: "node_modules/**", sourceMap: false }),
      replace({
        "process.env.NODE_ENV": JSON.stringify("production")
      }),
      terser({
        compress: {
          pure_getters: true,
          unsafe: true,
          unsafe_comps: true,
          warnings: false
        }
      })
    ]
  }
];
